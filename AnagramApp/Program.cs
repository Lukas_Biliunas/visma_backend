﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WordsParser;
using AnagramSolver;

namespace AnagramApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"C:\Users\lukas.biliunas\Desktop\Uzduotis 1\zodynas.txt";
            WordsReader wordsreader = new WordsReader();
            WordService wordService = new WordService();
            AnagramCreator anagramCreator = new AnagramCreator();
            string userInput;
            string userLetters;
            HashSet<string> readedWords = new HashSet<string>();
            List<string> alphabeticalLetters = new List<string>();
            Dictionary<string, HashSet<string>> wordsDictionary = new Dictionary<string, HashSet<string>>();


            
            readedWords = wordsreader.GetAllWords(path);

            foreach (string word in readedWords)
            {
                alphabeticalLetters.Add(wordService.SortAlphanumerically(word));
            }

            wordsDictionary = wordService.createDictionary(readedWords, alphabeticalLetters);
            Console.WriteLine("Enter string:");
            userInput = Console.ReadLine().ToString();

            userLetters = wordService.SortAlphanumerically(userInput);

            anagramCreator.GenerateAnagrams(userLetters);
        }
    }
}
