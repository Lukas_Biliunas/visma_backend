﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace WordsParser
{
    public class WordsReader : IRepository
    {
        public HashSet<string> GetAllWords(string path)
        {
            List<string> words = new List<string>();
            HashSet<string> hash = new HashSet<string>();

            try
            {
                string[] splittedString;

                foreach (string line in File.ReadLines(path))
                {
                    if (Char.IsLetter(line[0]))
                    {
                        splittedString = line.Split('\t');
                        words.Add(splittedString[0].ToLower());
                        words.Add(splittedString[2].ToLower());
                    }
                }
                return hash = new HashSet<string>(words);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return hash;
        }
    }
}
