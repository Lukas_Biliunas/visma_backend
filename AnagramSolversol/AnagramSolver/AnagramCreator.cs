﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using System.Configuration;


namespace AnagramSolver
{
    public class AnagramCreator : IAnagramGenerator
    {
        public List<string> GenerateAnagrams(string word)
        {
            List<string> generatedAnagrams = new List<string>();
            string anagram = "";
            string serviceString = "";
            int i = 0;
            int j;
            bool isInt;
            int minLength, maxResult;

            isInt = int.TryParse(ConfigurationManager.AppSettings["minLength"], out minLength);
            isInt = int.TryParse(ConfigurationManager.AppSettings["maxResult"], out maxResult);

            while (i < word.Length - minLength)
            {
                j = 1;
                anagram = word[i].ToString();
                while(j < word.Length - i - minLength)
                {
                    if (anagram.Length < minLength)
                    {
                        anagram += word[i + 1];
                    }
                    else
                    {
                        anagram.Remove(anagram.Length - 1);
                        anagram += word[i];
                    }
                    generatedAnagrams.Add(anagram);
                    serviceString = word;

                    foreach (char letter in anagram)
                    {
                        int y = serviceString.Count(x => x == letter);
                        if (y > 1)
                        {
                            serviceString = serviceString.Replace(letter.ToString(), "");
                            serviceString += letter;
                        }
                        else
                        {
                            serviceString = serviceString.Replace(letter.ToString(), "");
                        }
                    }
                    //anagram += word[i + 1].ToString();
                    generatedAnagrams.Add(serviceString);
                    j++;
                }
                i++;
            }

             return generatedAnagrams;
        }
    }
}
