﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver
{
    public class WordService
    {
        public string SortAlphanumerically(string word)
        {
            string modifiedWord;

            char[] characters = word.ToArray();
            Array.Sort(characters);
            modifiedWord = new string(characters);

            return modifiedWord;
        }

        public Dictionary<string, HashSet<string>> createDictionary(HashSet<string> words, List<string> modifiedWords)
        {
            Dictionary<string, HashSet<string>> wordsDictionary = new Dictionary<string, HashSet<string>>();
            int i = 0;
            HashSet<string> hash = new HashSet<string>();
            List<string> wordsList = new List<string>(words.ToList());

            foreach (string word in words)
            {
                if (!wordsDictionary.ContainsKey(modifiedWords[i]))
                {
                    wordsDictionary.Add(modifiedWords[i], new HashSet<string>());
                }
                wordsDictionary[modifiedWords[i]].Add(word);

                i++;
            }
            return wordsDictionary;
        }
    }
}
